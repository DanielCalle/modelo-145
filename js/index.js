$(document).ready(function(){
        // Step show event 
    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
        //alert("You are on step "+stepNumber+" now");
        if(stepPosition === 'first'){
            $("#prev-btn").addClass('disabled');
        }else if(stepPosition === 'final'){
            $("#next-btn").addClass('disabled');
        }else{
            $("#prev-btn").removeClass('disabled');
            $("#next-btn").removeClass('disabled');
        }
     });
     
     // Toolbar extra buttons
     var btnFinish = $('<button></button>').text('Finish')
                                      .addClass('btn btn-info')
                                      .on('click', function(){ alert('Finish Clicked'); });
     var btnCancel = $('<button></button>').text('Cancel')
                                      .addClass('btn btn-danger')
                                      .on('click', function(){ $('#smartwizard').smartWizard("reset"); });                         
     
     
     // Smart Wizard
     $('#smartwizard').smartWizard({ 
             selected: 0, 
             theme: 'dots',
             transitionEffect:'fade',
             showStepURLhash: true,
             toolbarSettings: {toolbarPosition: 'bottom',
                               toolbarExtraButtons: [btnCancel]
                             }
     });
                                  
     
     // External Button Events
     $("#reset-btn").on("click", function() {
         // Reset wizard
         $('#smartwizard').smartWizard("reset");
         return true;
     });
     
     $("#prev-btn").on("click", function() {
         // Navigate previous
         $('#smartwizard').smartWizard("prev");
         return true;
     });
     
     $("#next-btn").on("click", function() {
         // Navigate next
         $('#smartwizard').smartWizard("next");
         return true;
     });
     
     // Set selected theme on page refresh
     $("#theme_selector").change();
 
     $('.date-own').datepicker({
         
         minViewMode: 2,
         
         format: 'yyyy'
         
     });
        // With JQuery
    $("#ex6").slider();
    $("#ex6").on("slide", function(slideEvt) {
        $("#ex6SliderVal").text(slideEvt.value);
    });

    $('#chanceSlider').on('change', function(){
        $('#chance').val($('#chanceSlider').val());
    });
    
    $('#chance').on('keyup', function(){
        $('#chanceSlider').val($('#chance').val());
    });

    $("#ex2").slider();
    $("#ex2").on("slide", function(slideEvt) {
        $("#ex2SliderVal").text(slideEvt.value);
    });

    $('input:radio').on('click', function(e) {
        if($('input[name = "optionsRadios"]:checked').val() == "option2") {
            $('#inputNIF').removeAttr('disabled');
            $('#divNIF').addClass("has-error");
        }
        else {
            $('#inputNIF').attr('disabled', '');
            $('#divNIF').removeClass("has-success");
            $('#divNIF').removeClass("has-error");
            $('#inputNIF').val('');
        }
    });

    $('#inputNIF').on('input', function() {
        console.log("hola");
        if($('#inputNIF').attr('disabled') == undefined) {
            if($('#inputNIF').val() == "") {
                console.log("danger");
                $('#divNIF').addClass("has-error");
                $('#divNIF').removeClass("has-success");
            }
            else {
                console.log("success");
                $('#divNIF').addClass("has-success");
                $('#divNIF').removeClass("has-error");
            }

        }

      });
      
    $("#ex1").slider();
    $("#ex1").on("slide", function(slideEvt) {
        $("#ex1SliderVal").text(slideEvt.value);
    });

});